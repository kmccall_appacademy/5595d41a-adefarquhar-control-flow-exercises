# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete!('a-z')
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  len = str.length
  len.even? ? str[len/2-1, 2] : str[len/2]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.count { |chr| VOWELS.include? chr }
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  prod = 1
  (1..num).each { |number| prod *= number }
  prod
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ''
  arr.each { |chr| joined += "#{chr}#{separator}" }
  joined[-1] == separator ? joined[0...-1] : joined
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str = str.downcase.chars
  str = str.each_index { |idx| str[idx] = str[idx].upcase! if idx.odd? }
  str.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split
  words = words.each { |word| word.reverse! if word.length > 4}.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fzbz = (1..n).to_a
  fzbz = fzbz.each_index do |idx|
    fzbz[idx] = 'fizzbuzz' if fzbz[idx] % 15 == 0
    fzbz[idx] = 'fizz' if fzbz[idx] % 3 == 0
    fzbz[idx] = 'buzz' if fzbz[idx] % 5 == 0
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  len = arr.length
  arr.each_index do |idx|
    arr[idx],arr[len-1+idx] = arr[len-1+idx],arr[idx] if idx < len/2
  end
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1

  (2..num/2).each { |div| return false if (num % div).zero? }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  fctr_arr = []
  (1..num).each { |fctr| fctr_arr << fctr if (num % fctr).zero? }
  fctr_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_fctrs = []
  (2..num).each do |nmbr|
    prime_fctrs << nmbr if prime?(nmbr) && (num % nmbr).zero?
  end
  prime_fctrs
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).size
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  if arr.first(3).count{|num| num.even? } > 1
    arr.each { |num| return num if num.odd? }
  else
    arr.each { |num| return num if num.even? }
  end
end
